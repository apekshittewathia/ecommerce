function AJAXHandler(filePath, successCallback, failureCallback) {
  this.filePath = filePath;
  this.successCallback = successCallback;
  this.failureCallback = failureCallback;
}

AJAXHandler.prototype.init = function() {
  var that = this;
  $.ajax({
    url: this.filePath,
    type: "GET",
    dataType: "json"
  }).done(function(response) {
    that.response = response;
    that.successCallback(response);
    console.log(response);
  }).fail(function(xhr, status, errorThrown) {
    that.successCallback();
    alert("Sorry, there was a problem!");
    console.log("Error: " + errorThrown);
    console.log("Status: " + status);
    console.log(xhr);
  });
};

//---------------------------------------------------------

function ProductsHandler(products) {
  this.products = products;
  this.$productsContainer = $("[data-type=products-container]");
}

ProductsHandler.prototype.updateProducts = function(products) {
  this.products = products;
  this.showProducts();
};

ProductsHandler.prototype.showProducts = function() {
  this.$productsContainer.empty();
  this.$productsContainer.append(this.getHTMLStructure());
};

ProductsHandler.prototype.getHTMLStructure = function() {
  var $productsDiv = $("<div data-type='products-div'/>");
  this.products.forEach(function(item, index, array) {
    var $productDiv = $("<div class='product-div'/>");
    $productDiv.append("<img src='product_data/images/" + item.url + "'>");
    $productDiv.append("<div>" + item.name + "</div>");
    $productDiv.append("<div>" + item.color + "</div>");
    $productDiv.append("<div>" + item.brand + "</div>");
    $productsDiv.append($productDiv);
  });
  return $productsDiv;
};

//---------------------------------------------------------

function FiltersHandler(products) {
  this.products = products;
  this.$filtersContainer = $("[data-type=filters-container]");
  var filterObject = this.getFilterObject("color", "brand", "sold_out");
  this.filters = {
    color: {
      values: Object.keys(filterObject.color),
      type: "String",
      name: "Colors"
    },
    brand: {
      values: Object.keys(filterObject.brand),
      type: "String",
      name: "Brands"
    },
    sold_out: {
      values: Object.keys(filterObject.sold_out),
      type: "Boolean",
      name: "Available"
    }
  };
  this.productsHandler = new ProductsHandler([]);
  console.log(this.filters);
}

FiltersHandler.prototype.getFilterObject = function(){
  var filterObject = {};
  for(var i = 0; i < arguments.length; i++){
    filterObject[arguments[i]] = {};
  }
  var args = arguments;
  this.products.forEach(function(item, index, array){
    var outerItem = item;
    for(var i = 0; i < args.length; i++)
      filterObject[args[i]][outerItem[args[i]]] = 1;
  });
  return filterObject;
};



// FiltersHandler.prototype.getUniqueFilters = function(filterType) {
//   return $.unique(this.products.map(function(item, index, array) {
//     return item[filterType]
//   }).sort());
// };

FiltersHandler.prototype.init = function() {
  this.$filtersContainer.append(this.getHTMLStructure());
  this.bindEvents();
  this.productsHandler.updateProducts(this.getFilteredProducts());
};


FiltersHandler.prototype.getHTMLStructure = function() {
  var filters = this.filters;
  var $filtersDiv = $("<div data-type='filters-div'/>");
  Object.keys(filters).forEach(function(item, index, array) {
    var $filterCheckboxes = $("<div data-type='filter-div' class='filter-div'/>");
    $filterCheckboxes.append("<div>" + filters[item]["name"] + "</div>");
    var filterType = item;
    if (filters[item]["type"] === "Boolean") {
      $filterCheckboxes.append("<input type='checkbox' value='" + item + "' name=" + item + " data-type=" + item + ">" + filters[item]["name"] + "<br>");
    } else {
      filters[item]["values"].forEach(function(item, index, array) {
        $filterCheckboxes.append("<input type='checkbox' value='" + item + "' name=" + filterType + " data-type=" + filterType + ">" + item + "<br>");
      });
    }
    $filterCheckboxes.append("<br>");
    $filtersDiv.append($filterCheckboxes);
  });
  return $filtersDiv;
};

FiltersHandler.prototype.getFilteredProducts = function() {
  var filters = this.filters;
  var filteredProducts = this.products.slice(0);
  var that = this;
  Object.keys(filters).forEach(function(item, index, array) {
    var filterType = item;
    var checkedElements = that.$filtersContainer.find("input[data-type=" + item + "]:checked");
    if (checkedElements.length === 0) {
      return;
    }
    if (filters[item]["type"] == "Boolean") {
      filteredProducts = filteredProducts.filter(function(item, index, array) {
        return item[filterType] == false;
      });
    } else {
      filteredProducts = filteredProducts.filter(function(item, index, array) {
        for (var i = 0; i < checkedElements.length; i++) {
          if (item[filterType] == checkedElements[i].value) {
            return true;
          }
        }
        return false;
      });
    }
  });
  return filteredProducts;
};

FiltersHandler.prototype.bindEvents = function() {
  var that = this;
  $("input[type=checkbox]").click(function() {
    that.productsHandler.updateProducts(that.getFilteredProducts());
  });
};

//---------------------------------------------------------

function Controller() {}

Controller.prototype.loadContent = function(response) {
  var filtersHandler = new FiltersHandler(response);
  filtersHandler.init();
};



$(document).ready(function() {
  var controller = new Controller();
  var ajaxHandler = new AJAXHandler("product.json", controller.loadContent, function(){});
  ajaxHandler.init();
});